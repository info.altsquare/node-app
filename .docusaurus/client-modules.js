export default [
  require("/node-app-deployment/node_modules/infima/dist/css/default/default.css"),
  require("/node-app-deployment/node_modules/@docusaurus/theme-classic/lib/prism-include-languages"),
  require("/node-app-deployment/node_modules/@docusaurus/theme-classic/lib/nprogress"),
  require("/node-app-deployment/src/css/custom.css"),
];
